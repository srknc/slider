import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';


const enum FORM_STATUS {
  'VALID' = 'VALID',
  'INVALID' = 'INVALID'
}

@Component({
  selector: 'logo-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {
  public formGroup: FormGroup;
  public errorFields: string[] = [];
  @Output() emitter: EventEmitter<number> = new EventEmitter<number>();

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.formCreator();
    this.formListener();
  }

  formCreator() {
    this.formGroup = this.fb.group({
      sliders: [1, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(2),
      ]],
      names: ['', [
        Validators.required,
        Validators.maxLength(5),
        Validators.minLength(2)
      ]]
    });
  }

  formListener() {
    this.formGroup.valueChanges.subscribe((data: any) => this.onFormChanged(data));
  }

  onFormChanged(data) {
    console.log(this.formGroup, data);
    this.findErrors();
    if (this.formGroup.controls.sliders.status === FORM_STATUS.VALID) {
      this.emitter.emit(this.formGroup.get('sliders').value);
    }
  }

  onSubmit(formGroup) {
    console.log('Form Değerlerini gönder ', formGroup);
  }

  findErrors() {
    this.errorFields = [];
    Object.keys(this.formGroup.controls).forEach((item) => {
      if (this.formGroup.controls[item].status === FORM_STATUS.INVALID) {
        this.errorFields.push(item);
      }
    });
  }
}
