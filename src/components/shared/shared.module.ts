import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

const MODULES = [CommonModule, FormsModule, ReactiveFormsModule];
const DECLARATIONS = [];

@NgModule({
  declarations: [DECLARATIONS],
  imports: [MODULES],
  exports: [MODULES, DECLARATIONS]
})
export class SharedModule {
}
