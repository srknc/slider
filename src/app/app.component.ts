import {Component} from '@angular/core';

@Component({
  selector: 'logo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public title = 'slider';
  public clicked = 0;
  public yazilan: string;
  public swipeValue = 0;
  public bulletCount = 1;

  onDeneme($event) {
    console.log('app onDeneme event: ', $event);
    this.clicked = $event;
  }

  onTyped($event) {
    console.log(event);
    this.yazilan = $event;
  }

  swipeEdildi($event) {
    this.swipeValue = $event;
  }

  onSettingChange($event) {
    this.bulletCount = $event;
  }
}
