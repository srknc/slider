import {ChangeDetectorRef, Component, Directive, EventEmitter, HostBinding, Input, OnChanges, OnInit, Output} from '@angular/core';
import {DomSanitizer, SafeStyle} from '@angular/platform-browser';
import {NgModel} from '@angular/forms';

import './slider.component.scss';

@Directive({selector: '[ngModel]'})
export class NgModelStatusDirective {
  constructor(public control: NgModel) {
  }

  @HostBinding('class.valid') get valid() {
    return this.control.valid;
  }

  @HostBinding('class.invalid') get invalid() {
    return this.control.invalid;
  }
}

@Component({
  selector: 'logo-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnChanges {
  @Input() items = 4;
  @Output() deneme: EventEmitter<number> = new EventEmitter<number>();
  @Output() swipeEvent: EventEmitter<number> = new EventEmitter<number>();
  @Output() typed: EventEmitter<object> = new EventEmitter<object>();
  itemList = [];
  selectedIndex = 1;
  prop = null;

  constructor(private cd: ChangeDetectorRef, private sanitizer: DomSanitizer) {
  }

  @HostBinding('style')
  get myStyle(): SafeStyle {
    return this.sanitizer.bypassSecurityTrustStyle(`--number: 8"; --item-var: "${this.items}"; --some-var: 60px;`);
  }

  ngOnChanges() {
    this.setItems();
  }

  setItems() {
    this.itemList = new Array(Number(this.items));
  }

  decrease() {
    this.selectedIndex = (this.selectedIndex !== 0) ? --this.selectedIndex : this.itemList.length - 1;
    this.swipeEvent.emit(this.selectedIndex);
  }

  increase() {
    this.selectedIndex = (this.selectedIndex !== this.itemList.length - 1) ? ++this.selectedIndex : 0;
    this.swipeEvent.emit(this.selectedIndex);
  }

  onClicked() {
    console.log('slider onclick event: ', this.selectedIndex);
    this.deneme.emit(this.selectedIndex);
  }

  onChangeInput() {
    console.log(this.prop);
    this.typed.emit({a: 1, b: 2});
  }
}
