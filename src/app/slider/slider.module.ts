import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SliderRoutingModule} from './slider-routing.module';
import {NgModelStatusDirective, SliderComponent} from './slider.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [SliderComponent, NgModelStatusDirective],
  imports: [CommonModule, SliderRoutingModule, FormsModule, ReactiveFormsModule],
  exports: [SliderComponent]
})
export class SliderModule {
}
