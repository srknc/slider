import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SliderModule} from './slider/slider.module';
import {SettingModule} from '../components/setting/setting.module';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, BrowserAnimationsModule, AppRoutingModule, SliderModule, SettingModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
